var mongoose = require('mongoose'),
	encrypt = require('../utilities/encryption');


var userSchema = mongoose.Schema({
	name: {type: String, required: '{PATH} is required!'},
	email: {type: String, required: '{PATH} is required!', unique: true},
	salt: {type: String, required: '{PATH} is required!'},
	hashed_pwd: {type: String, required: '{PATH} is required!'},
	roles: [String]
});

userSchema.methods = {
	validPassword: function (passwordToMatch) {
		return encrypt.hashPwd(this.salt, passwordToMatch) === this.hashed_pwd;
	},

	hasRole: function (role) {
		return this.roles.indexOf(role) > -1;
	},

	compareByEmail: function (userEmail) {
		if (this.email.toString() == userEmail)
			return true;
		else
			return false;
	}
};

var User = mongoose.model('User', userSchema);

function createDefaultUsers() {
	User.find({}).exec(function (err, collection) {
		if (collection.length === 0) {
			var salt, hash;

			salt = encrypt.createSalt();
			hash = encrypt.hashPwd(salt, 'igor');
			User.create({
				name: 'Igor',
				email: 'igorrmotta@gmail.com',
				salt: salt,
				hashed_pwd: hash,
				roles: ['admin']
			});
		}
	});
}

exports.createDefaultUsers = createDefaultUsers;