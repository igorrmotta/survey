var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = function () {
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function (email, password, done) {
            User.findOne({email: email}).exec(function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, false, {message: 'Incorrect username.'});
                }

                if (!user.validPassword(password)) {
                    return done(null, false, {message: 'Incorrect password'});
                }

                return done(null, user);

                //if (user && user.authenticate(password)) {
                //    return done(null, user);
                //}
                //else {
                //    return done(null, false);
                //}
            })
        }
    ));


    passport.serializeUser(function (user, done) {
        if (user) {
            done(null, user._id);
        }
    });

    passport.deserializeUser(function (id, done) {
        User.findOne({_id: id}).exec(function (err, user) {
            if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        })

    });
};