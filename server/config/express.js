var express = require('express'),
	logger = require('morgan'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	passport = require('passport');

module.exports = function (app, config) {
	app.set('views', config.rootPath + '/server/views');
	app.set('view engine', 'html');
	app.engine('html', require('hbs').__express);

	app.use(logger('dev'));

	app.use(cookieParser());
	app.use(bodyParser());
	app.use(session({secret: 'white unicorns'}));
	app.use(passport.initialize());
	app.use(passport.session());

	//all public req will be responded by public dir now
	app.use(express.static(config.rootPath + '/public'));
};