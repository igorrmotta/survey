var auth = require('./auth'),
	mongoose = require('mongoose'),
	users = require('../controllers/users'),
	User = mongoose.model('User'),
	path = require("path");

module.exports = function (app) {

	/*app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
	 app.post('/api/users', users.createUser)
	 app.put('/api/users', users.updateUser)

	 app.get('/partials/*', function (req, res) {
	 res.render('../../public/app/' + req.params[0]);
	 });

	 app.get('/app/*', function (req, res) {
	 res.render('../../public/app/' + req.params[0]);
	 });*/
	app.delete('/api/users', auth.requiresRole('admin'), users.deleteUser);
	app.post('/api/users', auth.requiresRole('admin'), users.createUser);
	app.get('/api/users', auth.requiresRole('admin'), users.getUsers);

	app.post('/login', auth.authenticate);

	app.post('/logout', function (req, res) {
		req.session.destroy();
		res.end();
	});


	app.get('/init', function (req, res) {
		if (req.session.user)
			res.json(
				{user: req.session.user}
			);
		else
			res.status(401).end();
	});

	app.get('/', function (req, res) {
		res.sendFile(path.join(__dirname + '../../views/index.html'));
	});

};