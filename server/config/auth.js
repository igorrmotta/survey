var passport = require('passport');

exports.authenticate = function (req, res, next) {
	req.body.email = req.body.email.toLowerCase();

	var auth = passport.authenticate('local', function (err, user, info) {
		if (err) {
			return next(err);
		}

		if (!user) {
			res.status(401);
			res.send({success: false, message: info.message});
		}

		req.logIn(user, function (err) {
			if (err) {
				return next(err);
			}

			req.session.user = user;

			res.send({success: true, user: user});
		})
	});
	auth(req, res, next);
};

exports.requireApiLogin = function (req, res, next) {
	if (!req.isAuthenticated()) {
		res.status(403);
		res.end();
	} else {
		next();
	}
};

exports.requiresRole = function (role) {
	return function (req, res, next) {
		if (!req.user || req.user.roles.indexOf(role) === -1) {
			res.status(403);
			res.end();
		} else {
			next();
		}
	}

};