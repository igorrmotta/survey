var User = require('mongoose').model('User'),
	encrypt = require('../utilities/encryption');

exports.getUsers = function (req, res) {
	if (!req.user || !req.user.hasRole('admin')) {
		res.status(403);
		return res.end();
	}

	User.find({}).exec(function (err, collection) {
		res.send(collection);
	});
};

exports.deleteUser = function (req, res) {
	if (!req.user || !req.user.hasRole('admin')) {
		return res.status(403).end();
	}

	if (req.user.compareByEmail(req.body.userToDelete.email)) {
		res.status(403).send({reason: 'You cannot delete a user that you are logged in'});
		return;
	}

	var userToDelete = req.body.userToDelete;

	User.remove({email: userToDelete.email}, function (err) {
		if (!err) {
			res.send('OK');
		}
		else {
			res.send(err);
		}
	})
};

exports.createUser = function (req, res, next) {
	var userData = req.body;
	userData.email = userData.email.toLowerCase();
	userData.salt = encrypt.createSalt();
	userData.hashed_pwd = encrypt.hashPwd(userData.salt, userData.password);

	User.create(userData, function (err, user) {
		if (err) {
			if (err.toString().indexOf('E11000') > -1) {
				err = new Error('Duplicated email');
			}
			res.status(400);
			return res.send({reason: err.toString()});
		}


		return res.status(200).send(user);
		/*req.logIn(user, function (err) {
		 if (err) {
		 return next(err);
		 }
		 res.send(user);
		 });*/

	})
};

exports.updateUser = function (req, res) {
	var userUpdates = req.body;

	if (req.user._id != userUpdates._id && !req.user.hasRole('admin')) {
		res.status(403);
		return res.end();
	}

	req.user.name = userUpdates.name;
	req.user.email = userUpdates.email;
	if (userUpdates.password && userUpdates.password.length > 0) {
		req.user.salt = encrypt.createSalt();
		req.user.hashed_pwd = encrypt.hashPwd(req.user.salt, userUpdates.password);
	}

	req.user.save(function (err) {
		if (err) {
			res.status(400);
			return res.send({reason: err.toString()});
		}

		res.send(req.user);
	});
};