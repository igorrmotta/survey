"use strict";

angular.module('app').directive('dashboardPage', [
	function () {
		return {
			templateUrl: 'app/dashboard/dashboardTemplate.html',
			controller: 'dashboardController'
		}
	}
]);