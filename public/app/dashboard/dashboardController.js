"use strict";

angular.module('app').controller('dashboardController',
	['$scope', '$window', 'mvIdentity',
		function ($scope, $window, mvIdentity) {
			$scope.title = 'Template Dashboard';

			$scope.gridsterOpts = {
				columns: 12,
				margins: [20, 20],
				outerMargin: false,
				pushing: true,
				floating: true,
				swapping: false
			};
		}
	]);