"use strict";

angular.module('app').controller('appController',
	['$rootScope', '$scope', '$http', 'mvAuth', 'mvIdentity',
		function ($rootScope, $scope, $http, mvAuth, mvIdentity) {
			$scope.state = 'unauthorized';

			$scope.signIn = function (email, password) {
				mvAuth.authenticateUser(email, password).then(
					function (user) {
						$scope.state = 'authorized';
					},
					function (reason) {
						$scope.loginErrorMessage = reason.message;
					},
					function (notification) {
						//TODO
					});
			};

			$scope.checkInit = function () {
				mvAuth.checkSession().then(
					function (user) {
						$scope.state = 'authorized';
					},
					function (reason) {
						$scope.state = 'unauthorized';
					},
					function (notification) {
						//TODO
					});
			};


			$scope.$on('onUserLogout', function () {
				$scope.state = 'unauthorized';
				$scope.loginErrorMessage = 'LOGOUT!';
				mvIdentity.currentUser = undefined;
			});

		}
	]);
