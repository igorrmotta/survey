"use strict";

angular.module('yingFramework').controller('yingUserProfileController',
	['$scope', '$rootScope', 'mvIdentity', '$modal',
		function ($scope, $rootScope, mvIdentity, $modal) {
			$scope.profileMenuOptions =
				[
					{
						title: 'My Profile',
						onClick: myProfileOnClick
					},
					{
						title: 'Logout',
						onClick: logoutOnClick
					}
				];


			function openModal(size) {
				var modalInstance = $modal.open({
						animation: $scope.animationsEnabled,
						templateUrl: '/app/yingFramework/yingUserProfile/userProfileDialogTemplate.html',
						controller: 'userProfileDialogController',
						scope: $scope,
						size: size
					}
				);

				modalInstance.result.then(function (user) {
				});
			}

			function myProfileOnClick(menuOpt) {
				openModal();
			}


			function logoutOnClick(menuOpt) {
				mvIdentity.currentUser.logout().then(
					function suc() {
						$scope.$emit('onUserLogout');
					},
					function err() {

					},
					function pro() {

					});
			}

			$scope.name = mvIdentity.currentUser.name;
			$scope.email = mvIdentity.currentUser.email;
		}
	]);