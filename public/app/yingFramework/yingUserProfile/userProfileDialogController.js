"use strict";

angular.module('app').controller('userProfileDialogController',
	['$scope', '$modalInstance', 'mvIdentity',
		function ($scope, $modalInstance, mvIdentity) {

			console.log(mvIdentity.currentUser);
			$scope.user = mvIdentity.currentUser;

			$scope.ok = function () {
				$modalInstance.close('ok');
			};

			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);