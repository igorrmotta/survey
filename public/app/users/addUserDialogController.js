"use strict";

angular.module('app').controller('addUserDialogController',
	['$scope', '$modalInstance',
		function ($scope, $modalInstance) {

			$scope.newUser = {};

			$scope.ok = function () {
				if (!$scope.newUser.email) {
					$scope.errorUserDialog = 'Check your email.';
					return;
				}

				if (!$scope.newUser.name) {
					$scope.errorUserDialog = 'Check your name.';
					return;
				}

				if (!$scope.newUser.password || !$scope.newUser.confirmPassword ||
					($scope.newUser.password != $scope.newUser.confirmPassword)) {
					$scope.errorUserDialog = 'Check your password.';
					return;
				}

				var roles = $scope.newUser.isAdmin ?
					['admin'] : [];

				$scope.newUser.roles = roles;
				$scope.newUser.isAdmin = undefined;//property just for the dialog

				$modalInstance.close($scope.newUser);
			};

			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);