"use strict";

angular.module('app').controller('usersController',
	['$scope', '$http', '$modal', 'mvNotifier', 'mvIdentity',
		function ($scope, $http, $modal, mvNotifier, mvIdentity) {
			$scope.usersList = [];
			$scope.userListError = "";

			$scope.animationsEnabled = true;

			function editUser(user) {
				mvNotifier.notify('edit');
			}

			function addUser(user) {
				$http.post('/api/users', user).then(
					function suc(response) {
						mvNotifier.notify('User added successfully');
						getUsersList();
					},
					function err(response) {
						if (response.data.reason)
							mvNotifier.error(response.data.reason);
						else
							console.log(response);
					},
					function notify() {
						//TODO
					}
				);
			}

			function deleteUser(user) {
				var config = {
					method: "DELETE",
					url: '/api/users',
					data: {
						userToDelete: user
					},
					headers: {"Content-Type": "application/json;charset=utf-8"}
				};

				$http(config).then(
					function suc(response) {
						mvNotifier.notify('User deleted successfully');
						getUsersList();
					},
					function err(response) {
						if (response.data.reason)
							mvNotifier.error(response.data.reason);
						else
							console.log(response);
					},
					function notify() {

					}
				);
			}

			function openAddModal(size) {
				var modalInstance = $modal.open({
						animation: $scope.animationsEnabled,
						templateUrl: '/app/users/addUserDialogTemplate.html',
						controller: 'addUserDialogController',
						scope: $scope,
						size: size
					}
				);

				modalInstance.result.then(function (user) {
					addUser(user);
				});
			}

			function openDeleteModal(user, size) {
				var modalInstance = $modal.open({
						animation: $scope.animationsEnabled,
						templateUrl: '/app/users/deleteUserDialogTemplate.html',
						controller: 'deleteUserDialogController',
						size: size,
						resolve: {
							user: function () {
								return user;
							}
						}
					}
				);

				modalInstance.result.then(function () {
					deleteUser(user);
				});
			}

			function getUsersList() {
				$http.get('/api/users').
					then(function (response) {
						$scope.usersList = response.data;
					}, function (err) {
						//TODO tratar melhor este erro
						$scope.userListError = err;
					}, function (notify) {
						//TODO
					});
			}

			$scope.onAddClicked = function () {
				openAddModal();
			};

			$scope.onDeleteClicked = function (user) {
				openDeleteModal(user);
			};

			$scope.onEditClicked = function (user) {
				editUser();
			};


			getUsersList();
		}
	])
;