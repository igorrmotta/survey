"use strict";

angular.module('app').controller('deleteUserDialogController',
	['$scope', '$modalInstance', 'user',
		function ($scope, $modalInstance, user) {

			console.log(user);
			$scope.user = user;

			$scope.ok = function () {
				$modalInstance.close();
			};

			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);