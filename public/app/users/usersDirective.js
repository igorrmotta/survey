"use strict";

angular.module('app').directive('usersPage', [
	function () {
		return {
			scope: {},
			templateUrl: 'app/users/usersTemplate.html',
			controller: 'usersController'
		}
	}
]);