"use strict";

angular.module('app').config(['$provide',
	function ($provide) {

		$provide.decorator("$exceptionHandler", ["$delegate", function ($delegate) {
			return function (exception, cause) {
				$delegate(exception, cause);
				//alert(exception.message);
			};
		}]);
	}
]);