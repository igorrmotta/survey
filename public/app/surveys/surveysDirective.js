"use strict";

angular.module('app').directive('surveysPage', [
	function () {
		return {
			scope: {},
			templateUrl: 'app/surveys/surveysTemplate.html',
			controller: 'surveysController'
		}
	}
]);