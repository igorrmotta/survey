"use strict";

angular.module('app').factory('mvAuth',
	['$http', '$q', 'mvIdentity', 'mvUser',
		function ($http, $q, mvIdentity, mvUser) {
			return {

				authenticateUser: function (email, password) {
					var dfd = $q.defer();

					$http.post('/login', {email: email, password: password})
						.then(
						function success(response) {
							if (response.data.success) {
								var user = new mvUser();
								angular.extend(user, response.data.user);
								mvIdentity.currentUser = user;
								dfd.resolve(mvIdentity.currentUser);
							}
							else {
								dfd.reject(response.data);
							}
						},
						function failure(response) {
							dfd.reject(response.data);
						});

					return dfd.promise;
				},

				checkSession: function () {
					var dfd = $q.defer();

					$http.get('/init').
						then(function (response) {
							// this callback will be called asynchronously
							// when the response is available
							if (response.data.user) {
								var user = new mvUser();
								angular.extend(user, response.data.user);
								mvIdentity.currentUser = user;
								dfd.resolve(mvIdentity.currentUser);
							}
						}, function (response) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
							dfd.reject(response.data);
						});

					return dfd.promise;
				}

			}
		}
	]);