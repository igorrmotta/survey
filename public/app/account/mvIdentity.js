"use strict";

angular.module('app').factory('mvIdentity',
	['$window', 'mvUser', '$q',
		function ($window, mvUser, $q) {
			var currentUser;

			return {
				currentUser: currentUser,
				isAuthenticated: function () {
					return !!this.currentUser;
				},
				isAuthorized: function (role) {
					return !!this.currentUser && this.currentUser.roles.indexOf(role) > -1;
				}
			}
		}
	]);