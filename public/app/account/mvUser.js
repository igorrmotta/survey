"use strict";

angular.module('app').factory('mvUser',
	['$resource', '$q', '$http',
		function ($resource, $q, $http) {

			var UserResource = $resource('/api/users/:id', {_id: "@id"}, {
				update: {method: 'PUT', isArray: false}
			});

			UserResource.prototype.isAdmin = function () {
				return this.roles && this.roles.indexOf('admin') > -1;
			};

			UserResource.prototype.logout = function () {
				var dfd = $q.defer();
				$http.post('/logout').then(
					function suc(response) {
						dfd.resolve(response.data);
					},
					function err(response) {
						dfd.reject(response.data);
					}
				);
				return dfd.promise;
			};

			return UserResource;
		}
	]);