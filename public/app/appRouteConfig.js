"use strict";

angular.module('app').config(
	['$routeProvider',
		function ($routeProvider) {

			var routes = [
				{
					url: '/dashboard',
					config: {
						template: '<dashboard-page></dashboard-page>'
					}
				},
				{
					url: '/users',
					config: {
						template: '<users-page></users-page>'
					}
				},
				{
					url: '/surveys',
					config: {
						template: '<surveys-page></surveys-page>'
					}
				}
			];

			routes.forEach(function (route) {
				$routeProvider.when(route.url, route.config);
			});

			$routeProvider.otherwise({redirectTo: '/dashboard'});
		}
	]);