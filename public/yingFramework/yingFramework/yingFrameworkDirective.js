'use strict';

angular.module('yingFramework').directive('yingFramework', function () {
    return {
        transclude: true,
        scope: {
            title: '@',
            subtitle: '@',
            iconFile: '@'
        },
        controller: 'yingFrameworkController',
        templateUrl: 'yingFramework/yingFramework/yingFrameworkTemplate.html'
    };
});