"use strict";

angular.module('yingFramework').directive('yingUserProfile', function () {
	return {
		templateUrl: 'yingFramework/yingFramework/yingUserProfile/yingUserProfileTemplate.html',
		controller: 'yingUserProfileController'
	}
});