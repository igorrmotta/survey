"use strict";

angular.module('yingFramework').directive('yingUserProfileSmall', function () {
	return {
		templateUrl: 'yingFramework/yingFramework/yingUserProfile/yingUserProfileSmallTemplate.html',
		controller: 'yingUserProfileController'
	}
});