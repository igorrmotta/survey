"use strict";

angular.module('yingDashboard').directive('yingDashboard', function () {
    return {
        templateUrl: 'yingFramework/yingDashboard/yingDashboardTemplate.html',
        link: function (scope, element, attrs) {
            scope.addNewWidget = function (widget) {
                var newWidget = angular.copy(widget.settings);
                scope.widgets.push(newWidget);
            }
        }
    };
});